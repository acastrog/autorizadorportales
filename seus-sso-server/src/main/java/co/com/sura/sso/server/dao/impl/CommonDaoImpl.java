package co.com.sura.sso.server.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import co.com.sura.sso.server.dao.CommonDao;
import co.com.sura.sso.server.dto.MenuResponse;
import co.com.sura.sso.server.model.Token;

@Repository
public class CommonDaoImpl implements CommonDao{

	@PersistenceContext
	private EntityManager em;
	
	@Override
	public List<MenuResponse> getMenu(String app, String user, String dni) {
		
		List<MenuResponse> toReturn = new ArrayList<>();
		
		StringBuilder sb = new StringBuilder();
		sb.append("select m.id,r.name, r.url, m.parent ");
		sb.append("FROM perfil_x_usuario_x_empresa pue ");
		sb.append("join perfil_x_recurso pr on pr.perfil = pue.perfil ");
		sb.append("join menu_x_recurso m on m.resource = pr.recurso ");
		sb.append("join recurso r on r.id=pr.recurso ");
		sb.append("join servicio s on r.service = s.id ");
		sb.append("where s.name= :app ");
		sb.append("and pue.usuario= :user ");
		sb.append("and pue.empresa= :dni ");
		sb.append("order by m.parent ");


		Query query = em.createNativeQuery(sb.toString());
		query.setParameter("user", user);
		query.setParameter("dni", dni);
		query.setParameter("app", app);
		
		List<Object[]> result = query.getResultList();
		
		result.stream().forEach(item -> {
			// Se valida que no tenga padre
			if(null==item[3] || item[3].toString().isEmpty()) {
				toReturn.add(new MenuResponse(Integer.parseInt(item[0].toString()), item[1].toString(), item[2].toString()));
			}else {
				// Se busca el padre y se inserta en el listado de hijos del menu padre
				MenuResponse parent = toReturn.stream().filter(menu-> menu.getId() == Integer.parseInt(item[3].toString())).findFirst().orElse(null);
				parent.getChildren().add(new MenuResponse(Integer.parseInt(item[0].toString()), item[1].toString(), item[2].toString()));
			}
			
		});
		
		
		return toReturn;
	}

	
	
	@Override
	public List<String> getCompanies(String user) {

		
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT distinct empresa ");
		sb.append("FROM perfil_x_usuario_x_empresa ");
		sb.append("WHERE usuario = :user ");
		
		Query query = em.createNativeQuery(sb.toString());
		query.setParameter("user", user);
		
		List<String> toReturn = query.getResultList();
		
		return toReturn;
	}


	@Override
	public int auth(String app, String user, String dni, String path) {
		
		StringBuilder sb = new StringBuilder();
		sb.append("select url ");
		sb.append("from perfil_x_usuario_x_empresa  pue ");
		sb.append("join perfil_x_recurso pr on pr.perfil = pue.perfil ");
		sb.append("join recurso r on r.id=pr.recurso ");
		sb.append("join servicio s on r.service = s.id ");
		sb.append("where s.name= :app ");
		sb.append("and pue.usuario= :user ");
		sb.append("and pue.empresa= :dni ");
		sb.append("and r.url= :path ");
		
		Query query = em.createNativeQuery(sb.toString());
		query.setParameter("user", user);
		query.setParameter("dni", dni);
		query.setParameter("app", app);
		query.setParameter("path", path);
		
		return query.getResultList().size();
	}
	
}
