package co.com.sura.sso.server.api;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import co.com.sura.sso.server.business.CommonBusiness;
import co.com.sura.sso.server.dto.GenericResponse;
import co.com.sura.sso.server.dto.MenuResponse;
import co.com.sura.sso.server.dto.RedirectDTO;
import co.com.sura.sso.server.dto.TokenRequest;

@RestController
@RequestMapping(value = "/v1/sso",produces = "application/json",consumes="application/json")
@CrossOrigin(origins="*")
public class API {
	
	@Autowired
	private CommonBusiness business;
	
	@RequestMapping(method = RequestMethod.POST, value = "/redirect",produces = "text/html;charset=UTF-8",consumes="application/x-www-form-urlencoded")
	public ModelAndView redirect(Map<String, Object> model,@RequestParam(value="token", required=false) String token, 
	        @RequestParam(value="user", required=false) String user, 
	        @RequestParam(value="callback", required=false) String callback) {
		
		model.put("token", token);
		model.put("user", user);
		model.put("callback", callback);
		model.put("companies", business.getCompanies(user));
		
		return new ModelAndView("index2",model);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/companies",produces = "text/html;charset=UTF-8",consumes="application/x-www-form-urlencoded")
	public void setCompanies(@RequestParam(value="token", required=false) String token, 
	        @RequestParam(value="user", required=false) String user, 
	        @RequestParam(value="callback", required=false) String callback,
	        @RequestParam(value="company", required=false) String company,
	        HttpServletResponse response,HttpServletRequest request) throws IOException {
		
		System.out.println("-------------------compines");
		
		business.putCompanies(user,token,callback,company,response);
	}
	
	
	
	
	
	@RequestMapping(method = RequestMethod.POST, value = "/menu/{app}")
	public List<MenuResponse> menu(@PathVariable String app, @RequestBody TokenRequest input,HttpServletResponse response,HttpServletRequest request) {
		return business.getMenu(app,input);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/auth/{app}")
	public GenericResponse auth(@PathVariable String app, @RequestBody TokenRequest input,HttpServletResponse response,HttpServletRequest request) {
		return business.auth(app,input,response);
	}

}
