package co.com.sura.sso.server.singleton;

import java.util.HashMap;
import java.util.Map;

import co.com.sura.sso.server.dto.TokenRequest;

public class TokenSession {

	private static TokenSession instance;
	private Map<String, TokenRequest> tokens = new HashMap<String, TokenRequest>();

	
	private TokenSession() {
		super();
	}

	public static synchronized TokenSession getInstance() {
		if (instance == null) {
			instance = new TokenSession();
		}
		return instance;
	}

	public Map<String, TokenRequest> getTokens() {
		return tokens;
	}

	public void setTokens(Map<String, TokenRequest> tokens) {
		this.tokens = tokens;
	}
	
}
