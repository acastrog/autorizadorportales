package co.com.sura.sso.server.business;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.sura.sso.server.dao.CommonDao;
import co.com.sura.sso.server.dto.GenericResponse;
import co.com.sura.sso.server.dto.MenuResponse;
import co.com.sura.sso.server.dto.TokenRequest;
import co.com.sura.sso.server.singleton.TokenSession;

@Service
public class CommonBusiness {
	
	@Autowired
	private CommonDao dao;

	
	public List<MenuResponse> getMenu(String app, TokenRequest input){

		TokenRequest session = TokenSession.getInstance().getTokens().get(input.getToken());
		
		if(null==session || null == session.getUser() || null == session.getDni()) {
			throw new SecurityException("Error obteniendo el men�. No se a seleccionado una empresa!");
		}
		
		List<MenuResponse> toReturn = dao.getMenu(app, session.getUser(), session.getDni());
		return toReturn;
	}

	public List<String> getCompanies(String user) {
		
		List<String> toReturn = dao.getCompanies(user);
		return toReturn;
	}

	
	public GenericResponse putCompanies(TokenRequest input, HttpServletResponse response) {

		TokenRequest session = TokenSession.getInstance().getTokens().get(input.getToken());
		
		if(null==session || null == session.getUser()) {
			throw new SecurityException("Error obteniendo el men�. No se a seleccionado una empresa!");
		}else {
			session.setDni(input.getDni());
		}

		return new GenericResponse("200", "Se configuro correctamente la empresa");
	}

	
	
	
	public GenericResponse auth(String app, TokenRequest input, HttpServletResponse response) {

		TokenRequest session = TokenSession.getInstance().getTokens().get(input.getToken());
		
		if(null==session || null == session.getUser() || null == session.getDni()) {
			response.setStatus(500);
			return new GenericResponse("500", "Error autorizando el recurso. No se a seleccionado una empresa!");
		}
		
		int row = dao.auth(app, session.getUser(), session.getDni() ,input.getPath() );
		
		if(row==0) {
			response.setStatus(401);
			return new GenericResponse("401", "Acceso denegado");
		}
		
		return new GenericResponse("200", "Acceso permitido");
	}

	public void putCompanies(String user, String token, String callback, String company,
			HttpServletResponse response) throws IOException {
		
		TokenRequest session = TokenSession.getInstance().getTokens().get(token);
		
		if(null==session ) {
			TokenSession.getInstance().getTokens().put(token, new TokenRequest(token, user, company));
		}else {
			session.setDni(company);
			session.setUser(user);
		}

		System.out.println(TokenSession.getInstance().getTokens());
		
		response.sendRedirect(callback);
		
	}
	
}
