package co.com.sura.sso.server.dto;

public class TokenRequest {

	private String token;
	private String user;
	private String dni;
	private String path;

	public TokenRequest() {
		super();
	}

	public TokenRequest(String token, String user, String dni, String path) {
		super();
		this.token = token;
		this.user = user;
		this.dni = dni;
		this.path = path;
	}

	public TokenRequest(String token, String user, String dni) {
		super();
		this.token = token;
		this.user = user;
		this.dni = dni;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

}
