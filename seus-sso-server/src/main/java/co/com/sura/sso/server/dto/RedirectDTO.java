package co.com.sura.sso.server.dto;

public class RedirectDTO {

	private String user;
	private String callback;
	private String token;

	public RedirectDTO() {
		super();
	}

	public RedirectDTO(String user, String callback, String token) {
		super();
		this.user = user;
		this.callback = callback;
		this.token = token;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getCallback() {
		return callback;
	}

	public void setCallback(String callback) {
		this.callback = callback;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@Override
	public String toString() {
		return "RedirectDTO [user=" + user + ", callback=" + callback + ", token=" + token + "]";
	}

}
