package co.com.sura.sso.server.dao;

import java.util.List;

import co.com.sura.sso.server.dto.MenuResponse;

public interface CommonDao {

	
	public List<MenuResponse> getMenu(String app, String user, String dni);
	public List<String> getCompanies(String user);
	public int auth(String app, String user, String dni, String path);
}
