package co.com.sura.sso.j2e;

import java.util.ArrayList;
import java.util.List;

public class MenuResponse {

	private int id;
	private String name;
	private String url;
	private List<MenuResponse> children;

	public MenuResponse() {
		super();
	}

	public MenuResponse(int id, String name, String url) {
		super();
		this.id = id;
		this.name = name;
		this.url = url;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public List<MenuResponse> getChildren() {
		if(null==children) {
			children=new ArrayList<>();
		}
		return children;
	}

	public void setChildren(List<MenuResponse> children) {
		this.children = children;
	}

	

}
