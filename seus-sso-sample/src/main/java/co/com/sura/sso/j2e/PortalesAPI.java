package co.com.sura.sso.j2e;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class PortalesAPI {

	private static String url = "http://localhost.suranet.com:8082/v1/sso";
	private static String menu = "menu/app";
	
	private static void setHeaders(HttpRequest request){
		request.contentType("application/json");
	}
	
	private static String getCookieValue(HttpServletRequest request, String cookieName) {
		if (request.getCookies() != null) {
			for (Cookie cookie : request.getCookies()) {
				if (cookieName.equals(cookie.getName())) {
					return cookie.getValue();
				}
			}
		}
		return null;
	}
	
	public static List<MenuResponse> menus(HttpServletRequest req, HttpServletResponse res){
		List<MenuResponse> toReturn = new ArrayList<>();
		TokenRequest toSend = new TokenRequest();
		toSend.setToken(getCookieValue(req, "appTagDllo"));
		
		HttpRequest request = HttpRequest.post("http://localhost.suranet.com:8082/v1/sso/menu/app");
		setHeaders(request);
		request.send(new Gson().toJson(toSend));
		int code = request.code();
		
		switch (code) {
		case 200:
			String response = request.body();
			toReturn = new Gson().fromJson(response, new TypeToken<List<MenuResponse>>(){}.getType());
			return toReturn;
		default:
			System.out.println("Ivalida Response code: " + code
					+ " body: " + request.body());
		}
		
		
		
		return toReturn;
		
	}
	
}
