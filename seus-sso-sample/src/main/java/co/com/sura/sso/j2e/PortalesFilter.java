package co.com.sura.sso.j2e;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;

import co.com.sura.sso.SecurityApi;
import co.com.sura.sso.SecurityContext;
import co.com.sura.sso.TokenInfo;
import co.com.sura.sso.WebContext;

public class PortalesFilter extends GenericFilter {
	
	private GenericResponse hasAccess(String token, String path) {
		
		TokenRequest toSend = new TokenRequest();
		toSend.setToken(token);
		toSend.setPath(path);
		
		HttpRequest request = HttpRequest.post("http://localhost.suranet.com:8082/v1/sso/auth/app");
		request.contentType("application/json;charset=UTF-8");
		request.send(new Gson().toJson(toSend));
		String body = request.body();
		System.out.println("================request.body()=========================");
		System.out.println("body: "+body);
		System.out.println("path: "+path);
		System.out.println("================FIN request.body()=========================");
		
		return new Gson().fromJson(body, GenericResponse.class);
	}
	
	
	

	public boolean doAuthorization(HttpServletRequest request, HttpServletResponse response,
			SecurityContext securityContext) {
		String token = securityContext.webContext().getCookieValue("appTagDllo");
		String path = "/"+securityContext.webContext().getPath();


		
		GenericResponse auth = hasAccess(token, path);
		
		switch (auth.getKey()) {
		case "200":
			return true;
		case "500":
			System.out.println("===========>500");
			
			String user = securityContext.getUserInformation().getUsername();
			String callback = securityContext.webContext().getFullRequestURL();
			
			String html  = "<html><body><form action='http://localhost.suranet.com:8082/v1/sso/redirect' method='post'><input type='hidden' name='token' value='"+token+"'><input type='hidden' name='user' value='"+user+"'><input type='hidden' name='callback' value='"+callback+"'>Redireccionando...<noscript>Su navegador no acepta javascript. Haga clic en Continuar. </button></noscript></form><script>document.forms[0].submit();</script></body></html>";			
			securityContext.webContext().setResponseStatus(HttpServletResponse.SC_UNAUTHORIZED);
			securityContext.webContext().writeResponseContent(html);
			
			System.out.println("Redireccionando para la selecci�n de empresas");
			
				
			return true;

		default:
			System.out.println("Codigo no valido");
			return false;
		}
		
		
	}

	
	
}
