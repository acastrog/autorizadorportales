package com.baeldung.springmustache.controller;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.baeldung.springmustache.model.Article;

import co.com.sura.sso.j2e.MenuResponse;
import co.com.sura.sso.j2e.PortalesAPI;

@Controller
public class GenericController {

    @GetMapping("/")
    public ModelAndView displayArticle(Map<String, Object> model,HttpServletRequest req, HttpServletResponse res) {

    	List<MenuResponse> menu = PortalesAPI.menus(req, res);
    	
        List<Article> articles = IntStream.range(0, 10)
          .mapToObj(i -> generateArticle("Article Title " + i))
          .collect(Collectors.toList());

        model.put("articles", articles);
        model.put("menu", menu);

        return new ModelAndView("index", model);
    }
    
    
    
    

    private Article generateArticle(String title) {
        Article article = new Article();

        article.setTitle(title);
        article.setBody(
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur faucibus tempor diam. In molestie arcu eget ante facilisis sodales. Maecenas porta tellus sapien, eget rutrum nisi blandit in. Mauris tempor auctor ante, ut blandit velit venenatis id. Ut varius, augue aliquet feugiat congue, arcu ipsum finibus purus, dapibus semper velit sapien venenatis magna. Nunc quam ex, aliquet at rutrum sed, vestibulum quis libero. In laoreet libero cursus maximus vulputate. Nullam in fermentum sem. Duis aliquam ullamcorper dui, et dictum justo placerat id. Aliquam pretium orci quis sapien convallis, non blandit est tempus.");
        article.setAuthor("Jose Nel");
        return article;
    }
}


