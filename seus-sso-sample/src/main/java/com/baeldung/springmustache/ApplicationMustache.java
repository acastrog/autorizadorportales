package com.baeldung.springmustache;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mustache.MustacheEnvironmentCollector;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;

import com.samskivert.mustache.Mustache;

import co.com.sura.sso.j2e.PortalesFilter;

@SpringBootApplication
@ComponentScan(basePackages = {"com.baeldung"})
public class ApplicationMustache {

    public static void main(String[] args) {
        SpringApplication.run(ApplicationMustache.class, args);
    }

    @Bean
    public Mustache.Compiler mustacheCompiler(Mustache.TemplateLoader templateLoader, Environment environment) {

        MustacheEnvironmentCollector collector = new MustacheEnvironmentCollector();
        collector.setEnvironment(environment);

        return Mustache.compiler()
          .defaultValue("Some Default Value")
          .withLoader(templateLoader)
          .withCollector(collector);

    }
    
    
    @Bean
	public FilterRegistrationBean SAML2SPFilterRegistration() {
		FilterRegistrationBean registration = new FilterRegistrationBean();
		
		registration.setFilter(new PortalesFilter());
		registration.addUrlPatterns("/*");
        registration.addInitParameter("service", "AdministracionAfilinea");
		
		
		registration.addInitParameter("publicResources: 401", "/css/.*");
		registration.addInitParameter("publicResources: favicon", "/favicon.ico");
		registration.addInitParameter("publicResources: webjar", "/webjars/bootstrap/3.3.7/css/bootstrap.min.css");
		
		
		registration.setName("securityFilter");
		registration.setOrder(1);

		return registration;
	}
}

