package com.baeldung.springmustache.model;

import java.util.ArrayList;
import java.util.List;

public class Menu {

	private int id;
	private String name;
	private String url;
	private List<Menu> children;

	public Menu() {
		super();
	}

	public Menu(int id, String name, String url) {
		super();
		this.id = id;
		this.name = name;
		this.url = url;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public List<Menu> getChildren() {
		if(null==children) {
			children=new ArrayList<>();
		}
		return children;
	}

	public void setChildren(List<Menu> children) {
		this.children = children;
	}

	

}
